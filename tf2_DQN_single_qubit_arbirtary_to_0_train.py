# -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 10:29:29 2021
用 tf2 写一个从 任意态 态制备到 |0> 态的 DQN 算法
训练时，用 tf2 ，验证时 用矩阵方法
@author: Waikikilick
"""

import  os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2' #设置 tf 的报警等级
import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers,optimizers,losses,initializers,Sequential,metrics,models
import copy 
from collections import deque
import random
from scipy.linalg import expm
from time import *

tf.random.set_seed(1)
np.random.seed(1)
random.seed (1)

class Agent(object):
    def __init__(self, 
            n_actions=4,
            n_features=4,
            learning_rate=0.0001,
            reward_decay=0.9,
            e_greedy=0.99,
            replace_target_iter=250,
            memory_size=2000,
            batch_size=32,
            e_greedy_increment=None):
        
        self.n_actions = n_actions
        self.n_features = n_features
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon_max = e_greedy
        self.replace_target_iter = replace_target_iter
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.epsilon_increment = e_greedy_increment
        self.epsilon = 0 if self.epsilon_increment is not None else self.epsilon_max
        self.learn_step_counter = 0
        self.memory = deque(maxlen=self.memory_size)
        self.memory_counter = 0
        
    def create_model(self): # 创建模型
    
        model = tf.keras.Sequential() # 模型搭建
        model.add(tf.keras.layers.Dense(32, input_dim=self.n_features, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        model.add(tf.keras.layers.Dense(32, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        model.add(tf.keras.layers.Dense(self.n_actions, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        
        model.compile(loss='mse',optimizer=tf.keras.optimizers.RMSprop(learning_rate=self.lr)) # 模型装配
        return model
        
    def save_models(self): # 保存模型
    
        self.model.save('dqn_tf2_single_qubit_to_0_saved_model')
        self.target_model.save('dqn_tf2_single_qubit_to_0_saved_target_model')
        print('models saved !')
           
    def store_transition(self, s, a, r, s_): # 保存 记忆单元
    
        self.memory.append((s, a, r, s_)) 
        self.memory_counter += 1

    def choose_action(self, observation, tanxin): # tanxin 的值代表着 预测动作的 策略选择
      # tanxin = 0 意味着完全随机取动作
      # tanxin = 0.5 意味着执行动态贪心策略
      # tanxin = 1 意味着完全靠网络选择动作
        
        if tanxin == 0: # tanxin = 0, 意味着完全随机选动作
            action = np.random.randint(0, self.n_actions)
        elif tanxin == 1: # tanxin = 1, 意味着全靠网络预测动作, 通常在测试阶段用 # 其他值的话就意味着动作选择概率处于动态调整中, 比如 tanxin = 0.5
            demo = tf.reshape(observation,(1,4))	
            action = np.argmax(self.model.predict(demo)[0])
        else:
            if np.random.uniform() < self.epsilon:
                demo = tf.reshape(observation,(1,4))	
                action = np.argmax(self.model.predict(demo)[0])
            else:
                action = np.random.randint(0, self.n_actions)
            
        return action        
        
        
    def learn(self):
        
        if self.learn_step_counter % self.replace_target_iter == 0: # 每 replace_target_iter 次学习, 更新 target 网络参数一次
            self.target_model.set_weights(self.model.get_weights()) # target 网络参数从 主网络完全复制而来
                    
        if self.memory_counter < self.batch_size: # 只有当 记忆库中的数据量 比 批大小 多的时候才开始学习
            return # 如果 if 成立, 那么 return 后面的 且与 if 同级的代码不再执行
                
        batch_memory = random.sample(self.memory, self.batch_size) # 从 记忆库 中随机选出 批大小 数量的样本
        s_batch = np.array([replay[0] for replay in batch_memory]) # 多行数组
        demo_s_batch = tf.reshape(s_batch,(self.batch_size,4))
        Q = self.model.predict(demo_s_batch)
        
        next_s_batch = np.array([replay[3] for replay in batch_memory])
        demo_next_s_batch = tf.reshape(next_s_batch,(self.batch_size,4))
        Q_next = self.target_model.predict(demo_next_s_batch)

        # 使用公式更新训练集中的Q值
        for i, replay in enumerate(batch_memory):
            _, a, reward, _ = replay
            Q[i][a] =   reward + self.gamma * np.amax(Q_next[i]) # Q 值计算法则
 
        history = self.model.fit(demo_s_batch, Q, verbose=0) # 传入网络进行训练 # history 是记录每次训练时的 损失函数 字典 
            
        self.epsilon = self.epsilon + self.epsilon_increment if self.epsilon < self.epsilon_max else self.epsilon_max
        self.learn_step_counter += 1
      

class env(object):
    def  __init__(self, 
        action_space = [0,1], #允许的动作，默认两个分立值，只是默认值，真正值由调用时输入
        dt = 0.1,
        noise_a = 0,
        ): 
        self.action_space = action_space
        self.n_actions = len(self.action_space)
        self.n_features = 4 #描述状态所用的长度
        self.target_psi =  np.mat([[1], [0]], dtype=complex) #最终的目标态为 |0>,  np.array([1,0,0,0])
        self.s_x = np.mat([[0, 1], [1, 0]], dtype=complex)
        self.s_z = np.mat([[1, 0], [0, -1]], dtype=complex)
        self.dt = dt
        self.training_set, self.validation_set, self.testing_set = self.psi_set()
        
        self.noise_a = noise_a
        self.noise_normal = np.array([ 1.62434536, -0.61175641, -0.52817175, -1.07296862,  0.86540763,
       -2.3015387 ,  1.74481176, -0.7612069 ,  0.3190391 , -0.24937038,
        1.46210794, -2.06014071, -0.3224172 , -0.38405435,  1.13376944,
       -1.09989127, -0.17242821, -0.87785842,  0.04221375,  0.58281521,
       -1.10061918,  1.14472371,  0.90159072,  0.50249434,  0.90085595,
       -0.68372786, -0.12289023, -0.93576943, -0.26788808,  0.53035547,
       -0.69166075, -0.39675353, -0.6871727 , -0.84520564, -0.67124613,
       -0.0126646 , -1.11731035,  0.2344157 ,  1.65980218,  0.74204416,
       -0.19183555])
        #noise_normal 为均值为 0 ，标准差为 1 的正态分布的随机数组成的数组
        #该随机数由 np.random.seed(1) 生成: np.random.seed(1) \ noise_uniform = np.random.normal(loc=0.0, scale=1.0, size=41)
        self.noise = self.noise_a * self.noise_normal #uniform
        
        
    def psi_set(self):
        
        theta_num = 6 # 除了 0 和 Pi 两个点之外，点的数量
        varphi_num = 21 # varphi 角度一圈上的点数
        # 总点数为 theta_num * varphi_num + 2(布洛赫球两极) # 6 * 21 + 2 = 512
        
        theta = np.delete(np.linspace(0,np.pi,theta_num+1,endpoint=False),[0])
        varphi = np.linspace(0,np.pi*2,varphi_num,endpoint=False) 
        
        psi_set = []
        for ii in range(theta_num):
            for jj in range(varphi_num):
                psi_set.append(np.mat([[np.cos(theta[ii]/2)],[np.sin(theta[ii]/2)*(np.cos(varphi[jj])+np.sin(varphi[jj])*(0+1j))]]))
        psi_set.append(np.mat([[1], [0]], dtype=complex))
        psi_set.append(np.mat([[0], [1]], dtype=complex))
        random.shuffle(psi_set) # 打乱点集
    
        training_set = psi_set[0:32]
        validation_set = psi_set[32:64]
        testing_set = psi_set[64:128]
        
        return training_set, validation_set, testing_set
        
    
    def reset(self, init_psi): # 在一个新的回合开始时，归位到开始选中的那个点上
        
        init_state = np.array([init_psi[0,0].real, init_psi[1,0].real, init_psi[0,0].imag, init_psi[1,0].imag])
        # np.array([1实，2实，1虚，2虚])
        return init_state
    
    
    def step(self, state, action, nstep):
        
        psi = np.array([state[0:int(len(state) / 2)] + state[int(len(state) / 2):int(len(state))] * 1j])
        #array([[1实 + 1虚j, 2实 + 2虚j]])
        
        psi = psi.T
        #array([[1实 + 1虚j],
        #        2实 + 2虚j]])

        psi = np.mat(psi) 
        #matrix([[ 1实 + 1虚j],
        #        [ 2实 + 2虚j]])
        
        H =  float(action)* self.s_z + 1 * self.s_x
        U = expm(-1j * H * self.dt) 
        psi = U * psi  # next state

        err = 10e-4
        fid = (np.abs(psi.H * self.target_psi) ** 2).item(0).real  
        rwd = (fid)
        
        done = (((1 - fid) < err) or nstep >= 2 * np.pi / self.dt) 

        #再将量子态的 psi 形式恢复到 state 形式。 # 因为网络输入不能为复数，否则无法用寻常基于梯度的算法进行反向传播
    
        psi = np.array(psi)
        psi_T = psi.T
        state = np.array(psi_T.real.tolist()[0] + psi_T.imag.tolist()[0]) 

        return state, rwd, done, fid      

def relu(vector):
    for i in range(len(vector)):
        vector[i] = np.maximum(0,vector[i])
    return vector

def agent_matrix(x):
    out = np.mat(x) * net[0].numpy() + net[1].numpy()
    out = relu(out)
    out = out * net[2].numpy() + net[3].numpy()
    out = relu(out)
    out = out * net[4].numpy() + net[5].numpy()
    out = relu(out)
    action = np.argmax(out)
    return action

def training(ep_max): 
    
    training_set = env.training_set
    validation_set = env.validation_set
    
    for i in range(ep_max):
        
        training_init_psi = random.choice(training_set)
        fid_max = 0
        print('--------------------------')
        print('训练中..., 当前回合为:', i)
        observation = env.reset(training_init_psi)
        nstep = 0
        
        while True:
            action = agent.choose_action(observation, 0.5) 
            observation_, reward, done, fid = env.step(observation, action, nstep)  
            nstep += 1
            fid_max = max(fid_max, fid)
            agent.store_transition(observation, action, reward, observation_)
            agent.learn()
            observation = observation_
                
            if done:
                break
            
        # if i % 1 == 32: # 每 x 个回合用验证集验证一下效果，动作全靠网络，保存最大保真度和最大奖励
        #     # print('阶段验证一下, 请稍等...')
        #     network = agent.model
        #     net = network.trainable_variables
            
        #     validation_fid_list = []
        #     validation_reward_tot_list = []
            
        #     for validation_init_psi in validation_set:
                
        #         validation_fid_max = 0
        #         validation_reward_tot = 0
                
        #         observation = env.reset(validation_init_psi)
        #         nstep = 0
                
        #         while True:
        #             action = agent_matrix(observation) 
        #             observation_, reward, done, fid = env.step(observation, action, nstep)
        #             nstep += 1
        #             validation_fid_max = max(validation_fid_max, fid)
        #             validation_reward_tot = validation_reward_tot + reward * (agent.gamma ** nstep)
        #             observation = observation_
                    
        #             if done:
        #                 break
        #         validation_fid_list.append(validation_fid_max)                
        #         validation_reward_tot_list.append(validation_reward_tot)
            
        #     validation_reward_history.append(np.mean(validation_reward_tot_list))
        #     validation_fid_history.append(np.mean(validation_fid_list))
            
        #     print('本回合验证集平均保真度: ', np.mean(validation_fid_list))
        #     # print('本回合验证集平均总奖励: ', np.mean(validation_reward_tot_list))

def testing(): # 测试 测试集中的点 得到 保真度 分布
    print('\n测试中, 请稍等...')
    
    testing_set = env.testing_set
    fid_list = []
    
    for test_init_psi in testing_set:
        
        fid_max = 0
        observation = env.reset(test_init_psi)
        nstep = 0 
        
        while True:
            action = agent_matrix(observation) 
            observation_, reward, done, fid = env.step(observation, action, nstep)  
            nstep += 1
            fid_max = max(fid_max, fid)
            observation = observation_
                
            if done:
                break
            
        fid_list.append(fid_max)
        
    return fid_list
                 
    
if __name__ == "__main__":
    
    dt = np.pi/10
    
    env = env(action_space = list(range(4)),   #允许的动作数 0 ~ 4-1 也就是 4 个
               dt = dt)
    
    agent = Agent(env.n_actions, env.n_features,
              learning_rate = 0.01,
              reward_decay = 0.9, 
              e_greedy = 0.95,
              replace_target_iter = 200,
              memory_size = 20000,
              e_greedy_increment = 0.001)
    
    # agent.model = agent.create_model()
    # agent.target_model = agent.create_model() #选择直接新建网络，如果之前没有训练好的网络 或者 想忽略原有网络 就采用此项。此项和后面关于新建网络的代码块相冲突。
    
    # 如果当前工作目录已有之前训练好的模型，就直接导入，如果没有，就创建
    folder = os.getcwd() #返回当前工作目录
    files_list = os.listdir(folder) #用于返回指定的文件夹包含的文件或文件夹的名字的列表
    if 'dqn_tf2_single_qubit_to_0_saved_model_' in files_list: #判断是否已有训练好的网络
        agent.model = keras.models.load_model('dqn_tf2_single_qubit_to_0_saved_model') #导入训练好的网络 
        agent.target_model = keras.models.load_model('dqn_tf2_single_qubit_to_0_saved_target_model')
                
    else:
        agent.model = agent.create_model()
        agent.target_model = agent.create_model()
        
    network = agent.model
    net = network.trainable_variables
        
    validation_reward_history = []
    validation_fid_history = []
        
    begin_training = time()

    # 训练模块
    # (1) 此模块可反复利用, 首次利用时， 也就是直接在脚本里运行此程序. 需要再次使用以继续训练时，就复制到工作台执行。
    # (2) 当首次调用此模块, ep_max 应以 x * 5 - 1 结尾. 而在继续训练时 ep_max 应为 x * 5 + 1, 以避免记录 保真度 和 奖励值 时出现混乱, 其中 x 为整数
    training(ep_max = 33)   
   
    end_training = time()
    
    training_time = end_training - begin_training
    
    print('\ntraining_time =',training_time)  
        
    print('各验证回合回合奖励记录为: ', validation_reward_history)
    print('各验证回合最大保真度记录为: ', validation_fid_history)

    # 测试
    testing_fid_list = testing()
    print('测试集平均保真度为：', np.mean(testing_fid_list))
    
# # 继续训练
# training(ep_max = )
# print('各验证回合回合奖励记录为: ', validation_reward_history)
# print('各验证回合最大保真度记录为: ', validation_fid_history)

# # 保存模型
# agent.save_models() 


# training_time = 85.96930861473083
# 各验证回合回合奖励记录为:  6.944415087125716
# [4.274240017638107, 5.1691988250474825, 4.310195735807574, 4.165802916520321, 4.8536114852508705, 
#   4.486178622339919, 5.332396556900321, 4.974100947390603, 5.361973845618897, 5.070615879455321, 
#   4.908823348002476, 5.435674693187173, 6.0732484897388375, 6.149881583674179, 6.009356293448615, 
#   6.521858427225746, 6.119193119883159, 6.072074886019999, 6.6328877464228295, 6.482106670741552, 
#   6.3997523618127685, 6.79945874419329, 6.746070220334858, 6.896761654552893, 6.30404242458701, 
#   6.835456260454825, 6.672805053782166, 6.606494996947868, 6.772752149516437, 6.507428732959561, 
#   6.627885002693953, 6.666852061417924, 6.63306550306609, 6.663509219856809, 6.699364017334403, 
#   6.621532529482507, 6.382752853044458, 6.354592898193518, 6.591108460744266, 6.498836510775143, 
#   6.593086125897974, 6.552950941946221, 6.512487591318598, 6.604894044128738, 6.582475034154091, 
#   6.648360912381144, 6.493421317207505, 6.677412638940268, 6.8366050191930245, 6.74547858665739, 
#   6.678139997857009, 6.813494643347384, 6.71124188327277, 6.397826847684609, 6.817160200627139, 
#   6.352243652275942, 6.50301496689776, 6.463507127951055, 6.84553597730898, 6.782143694852282, 
#   6.695368999638319, 6.117542660518476, 6.526020691249139, 6.855273881562725, 6.750285081765272, 
#   6.618200224231497, 6.557399383223569, 6.402942854624689, 6.557995609101739, 6.877664828761141, 
#   6.577105768746028, 6.940599009442878, 6.345799959677135, 6.491285967501578, 6.475039040572533, 
#   6.916083402567844, 6.403804635186692, 6.8884010340403, 6.700871075925523, 6.7600946102104675, 
#   6.42347714671596, 6.644487655668999, 6.944415087125716, 6.596636736431453, 6.903929565532063, 
#   6.522654011393447, 6.8966506728634975, 6.747846142011786, 6.761807739227795, 6.563646902025626, 
#   6.678954426018423, 6.501413202721022, 6.508574166434579, 6.548419930895914, 6.5228832595902215, 
#   6.478937323905375, 6.472813167719435, 6.620211219795554, 6.142280893898307, 6.4729402538553895, 6.555458671671232]
# 各验证回合最大保真度记录为: max: epi = 34 fid = 0.9972148443345237
# [0.7068679946059196, 0.8277964195387957, 0.7343959663922626, 0.9070862089980993, 0.8686853590956769, 
#   0.9139744840773814, 0.9059593310724974, 0.9590259951670772, 0.9236470571113248, 0.9260105531924556, 
#   0.9214316160378757, 0.9854513362620936, 0.9901755546459158, 0.9912532710655733, 0.9736028553607219, 
#   0.983310437943262, 0.9805353899112916, 0.9776360623442981, 0.9819592647089586, 0.9840110852840811, 
#   0.9771950776230214, 0.9903123544210075, 0.9941226511513579, 0.9898292055027943, 0.9893502920458794, 
#   0.9926011720776262, 0.986828160421197, 0.9761546794013785, 0.9747870667606602, 0.9933295157349429, 
#   0.9892615973097652, 0.9929728965647667, 0.9972148443345237, 0.9932322128955962, 0.9925780589880052, 
#   0.9954484114104898, 0.992599224107209, 0.9926279847090649, 0.9849197157897336, 0.9914105339435698, 
#   0.9935790608695132, 0.9941680417914932, 0.9963440862141046, 0.9926160774204653, 0.9947421664647966, 
#   0.9928955100278221, 0.9953156026403465, 0.995107113085681, 0.9934320855657921, 0.9932671325886424, 
#   0.9965005108809624, 0.98721797937202, 0.9914637450996926, 0.9964926843206141, 0.9858448316164274, 
#   0.9905475240122117, 0.9959061593123631, 0.9944373026716307, 0.9879422544266776, 0.9932920932615812, 
#   0.9934869505197161, 0.9929736185188234, 0.99126886869863, 0.9936447594532773, 0.9969828039469929, 
#   0.9959408380122352, 0.9953647826939005, 0.9938790342391457, 0.9945824716621848, 0.9960461695422225, 
#   0.9961426111925755, 0.9903699256550329, 0.9915104086868323, 0.9965077461085352, 0.9945194627814802, 
#   0.9944685046757725, 0.995830840435032, 0.9960882218232945, 0.9879243018710343, 0.9953742218683472, 
#   0.993135659765708, 0.9929172641229188, 0.9934810997251606, 0.9917414799816002, 0.9908054551450736, 
#   0.996382711990197, 0.9919577541835282, 0.9931258066263056, 0.995849777490934, 0.9967271191013592, 
#   0.9880507277896049, 0.9959007784342744, 0.9912282335182327, 0.990840907662779, 0.9917177521821956, 
#   0.9930299254930334, 0.9933826910276677, 0.9939795442307489, 0.9804837282258073, 0.9849183139065396, 0.9904665542399584]

# 测试集平均保真度为： 0.9962193054630456

# rewards: 4.27424002 5.16919883 4.31019574 4.16580292 4.85361149 4.48617862 5.33239656 4.97410095 5.36197385 5.07061588 4.90882335 5.43567469 6.07324849 6.14988158 6.00935629 6.52185843 6.11919312 6.07207489 6.63288775 6.48210667 6.39975236 6.79945874 6.74607022 6.89676165 6.30404242 6.83545626 6.67280505 6.606495   6.77275215 6.50742873 6.627885   6.66685206 6.6330655  6.66350922 6.69936402 6.62153253 6.38275285 6.3545929  6.59110846 6.49883651 6.59308613 6.55295094 6.51248759 6.60489404 6.58247503 6.64836091 6.49342132 6.67741264 6.83660502 6.74547859 6.67814    6.81349464 6.71124188 6.39782685 6.8171602  6.35224365 6.50301497 6.46350713 6.84553598 6.78214369 6.695369   6.11754266 6.52602069 6.85527388 6.75028508 6.61820022 6.55739938 6.40294285 6.55799561 6.87766483 6.57710577 6.94059901 6.34579996 6.49128597 6.47503904 6.9160834  6.40380464 6.88840103 6.70087108 6.76009461 6.42347715 6.64448766 6.94441509 6.59663674 6.90392957 6.52265401 6.89665067 6.74784614 6.76180774 6.5636469 6.67895443 6.5014132  6.50857417 6.54841993 6.52288326 6.47893732 6.47281317 6.62021122 6.14228089 6.4729402538553895 6.555458671671232

# fids: 0.70686799 0.82779642 0.73439597 0.90708621 0.86868536 0.91397448 0.90595933 0.959026   0.92364706 0.92601055 0.92143162 0.98545134 0.99017555 0.99125327 0.97360286 0.98331044 0.98053539 0.97763606 0.98195926 0.98401109 0.97719508 0.99031235 0.99412265 0.98982921 0.98935029 0.99260117 0.98682816 0.97615468 0.97478707 0.99332952 0.9892616  0.9929729  0.99721484 0.99323221 0.99257806 0.99544841 0.99259922 0.99262798 0.98491972 0.99141053 0.99357906 0.99416804 0.99634409 0.99261608 0.99474217 0.99289551 0.9953156  0.99510711 0.99343209 0.99326713 0.99650051 0.98721798 0.99146375 0.99649268 0.98584483 0.99054752 0.99590616 0.9944373  0.98794225 0.99329209 0.99348695 0.99297362 0.99126887 0.99364476 0.9969828  0.99594084 0.99536478 0.99387903 0.99458247 0.99604617 0.99614261 0.99036993 0.99151041 0.99650775 0.99451946 0.9944685  0.99583084 0.99608822 0.9879243  0.99537422 0.99313566 0.99291726 0.9934811  0.99174148 0.99080546 0.99638271 0.99195775 0.99312581 0.99584978 0.99672712 0.98805073 0.99590078 0.99122823 0.99084091 0.99171775 0.99302993  0.99338269 0.99397954 0.98048373 0.9849183139065396 0.9904665542399584