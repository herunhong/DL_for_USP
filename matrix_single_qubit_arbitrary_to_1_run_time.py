# -*- coding: utf-8 -*-
"""
Created on Fri Oct  1 18:38:08 2021
因 tf2 运行速度慢
将 神经网络 从 tf2 中解放出来
直接用 矩阵 来作为 神经网络
@author: Waikikilick
"""

import tensorflow as tf
from tensorflow.keras import layers, Sequential
import numpy as np
import random
import warnings
from tensorflow import keras
from scipy.linalg import expm
warnings.filterwarnings("ignore", category=DeprecationWarning)
from time import *

class env(object):
    def  __init__(self, 
        action_space = [0,1], #允许的动作，默认两个分立值，只是默认值，真正值由调用时输入
        dt = 0.1,
        ): 
        self.action_space = action_space
        self.n_actions = len(self.action_space)
        self.n_features = 4 #描述状态所用的长度
        self.target_psi =  np.mat([[0], [1]], dtype=complex) #最终的目标态为 |0>,  np.array([1,0,0,0])
        self.s_x = np.mat([[0, 1], [1, 0]], dtype=complex)
        self.s_z = np.mat([[1, 0], [0, -1]], dtype=complex)
        self.dt = dt
        self.training_set, self.validation_set, self.testing_set = self.psi_set()
        
        
        
    def psi_set(self):
        
        theta_num = 6 # 除了 0 和 Pi 两个点之外，点的数量
        varphi_num = 21 # varphi 角度一圈上的点数
        # 总点数为 theta_num * varphi_num + 2(布洛赫球两极) # 6 * 21 + 2 = 512
        
        theta = np.delete(np.linspace(0,np.pi,theta_num+1,endpoint=False),[0])
        varphi = np.linspace(0,np.pi*2,varphi_num,endpoint=False) 
        
        psi_set = []
        for ii in range(theta_num):
            for jj in range(varphi_num):
                psi_set.append(np.mat([[np.cos(theta[ii]/2)],[np.sin(theta[ii]/2)*(np.cos(varphi[jj])+np.sin(varphi[jj])*(0+1j))]]))
        psi_set.append(np.mat([[1], [0]], dtype=complex))
        psi_set.append(np.mat([[0], [1]], dtype=complex))
        random.shuffle(psi_set) # 打乱点集
    
        training_set = psi_set[0:32]
        validation_set = psi_set[32:64]
        testing_set = psi_set[64:128]
        
        return training_set, validation_set, testing_set
        
    
    def reset(self, init_psi): # 在一个新的回合开始时，归位到开始选中的那个点上
        
        init_state = np.array([init_psi[0,0].real, init_psi[1,0].real, init_psi[0,0].imag, init_psi[1,0].imag])
        # np.array([1实，2实，1虚，2虚])
        return init_state
    
    
    def step(self, state, action, nstep):
        
        psi = np.array([state[0:int(len(state) / 2)] + state[int(len(state) / 2):int(len(state))] * 1j])
        psi = psi.T
        psi = np.mat(psi) 
        
        H =  float(action)* self.s_z + 1 * self.s_x
        U = expm(-1j * H * self.dt) 
        psi = U * psi  # next state

        fid = (np.abs(psi.H * self.target_psi) ** 2).item(0).real  
                
        done = (fid>=0.9999 or nstep >= 20) 

        #再将量子态的 psi 形式恢复到 state 形式。 # 因为网络输入不能为复数，否则无法用寻常基于梯度的算法进行反向传播
    
        psi = np.array(psi)
        psi_T = psi.T
        state = np.array(psi_T.real.tolist()[0] + psi_T.imag.tolist()[0]) 

        return state, done, fid  
    
def relu(vector):
    for i in range(len(vector)):
        vector[i] = np.maximum(0,vector[i])
    return vector

def agent(x):
    out = np.mat(x) * net[0].numpy() + net[1].numpy()
    out = relu(out)
    out = out * net[2].numpy() + net[3].numpy()
    out = relu(out)
    out = out * net[4].numpy() + net[5].numpy()
    out = relu(out)
    action = np.argmax(out)
    return action

def testing_time(): # 测试 测试集中的点 得到 保真度 分布
    print('\n时间测试中, 请稍等...')
    
    data = []
    
    for test_init_psi in testing_set:
        
        fid_max = 0
        fid_list_tem = []
        observation = env.reset(test_init_psi)
        nstep = 0 
        start_time = time()
        while True:
            action = agent(observation) 
            observation, done, fid = env.step(observation, action, nstep)  
            nstep += 1
            fid_list_tem.append(fid)
            if done:
                end_time = time()
                break
        
        test_fid = max(fid_list_tem)
        max_point = fid_list_tem.index(max(fid_list_tem))
        test_time = (end_time - start_time)*max_point/len(fid_list_tem)
        data.append((test_time,test_fid))
        
    return data

if __name__ == "__main__":
    
    dt = np.pi/10
    network = keras.models.load_model('dqn_tf2_single_qubit_to_1_saved_model') #导入训练好的网络 
    net = network.trainable_variables
    env = env(action_space = list(range(4)),   #允许的动作数 0 ~ 4-1 也就是 4 个
               dt = dt)
    
    testing_set = env.testing_set   
    # print(testing_set)
    
    # 测试
    data = testing_time()
    data.sort()
    
    time = 0
    fid = 0
    time_list = []
    fid_list = []
    
    for i in data:
        time += i[0]
        time_list.append(i[0])
        fid += i[1]
        fid_list.append(i[1])
        
    print('the mean time is: ', time/len(testing_set))
    print('the mean fid is: ', fid/len(testing_set))

    for i in (data):
        print(i)  
        
    # print(np.array(time_list))
    # print(np.array(fid_list))
     
    # the mean time is:  0.011812571463458266
    # time: 0.00171552 0.00190474 0.00190512 0.00238101 0.00285694 0.00285717 0.00285717 0.00428626 0.0047619  0.00571421 0.00571435 0.0066665 0.00666666 0.00666682 0.00666706 0.00833829 0.00857132 0.00857152 0.00923032 0.00952403 0.0095263  0.00958484 0.00997326 0.01047618 0.01047618 0.01047656 0.01142842 0.01142842 0.01216234 0.01238094 0.01238227 0.01238537 0.01333332 0.01372074 0.01428553 0.01428553 0.0142857  0.01428587 0.01428604 0.01428774 0.01481397 0.01489496 0.01504548 0.01521156 0.01523263 0.01523645 0.01618988 0.01619046 0.01619065 0.01619085 0.01619085 0.01619104 0.01619374 0.01713937 0.01714182 0.01714243 0.01714284 0.01714284 0.01714611 0.01809522 0.01809673 0.01904783 0.01951717 0.02131121
    
    # the mean fid is:  0.9964542679232411
    # time: 0.99916844 0.99965978 0.99984038 0.9901086  0.99231775 0.98858732 0.9975105  0.99381051 0.99880566 0.99280555 0.99765244 0.99987521 0.9966357  0.99953344 0.9974452  0.99498225 0.99941496 0.99731526 0.99999593 0.99278047 0.99985953 0.99652133 0.99344541 0.99527369 0.9958567  0.99253546 0.99472701 0.99827913 0.99703149 0.9964433 0.99571227 0.99638255 0.99853191 0.99589674 0.98901691 0.99833266 0.99056168 0.99794511 0.99718866 0.99856982 0.99789508 0.99885245 0.99260883 0.99400994 0.9997518  0.9936059  0.99372621 0.99356309 0.99965814 0.9956845  0.99946666 0.99588158 0.99694587 0.99796665 0.99436269 0.99857471 0.99789857 0.99799271 0.99830829 0.99981913 0.99771312 0.99787433 0.99492277 0.99963341
    
    # (0.0017155238560267857, 0.9991684439385338)
    # (0.0019047373817080544, 0.9996597807227633)
    # (0.0019051233927408855, 0.999840380774583)
    # (0.002381006876627604, 0.9901085958000345)
    # (0.002856935773577009, 0.992317752160549)
    # (0.0028571741921561106, 0.9885873150099757)
    # (0.0028571741921561106, 0.997510503615761)
    # (0.004286255155290876, 0.9938105098516384)
    # (0.004761900220598493, 0.9988056574593126)
    # (0.005714212145124163, 0.9928055454310334)
    # (0.005714348384312221, 0.9976524442826976)
    # (0.006666501363118489, 0.9998752093521819)
    # (0.006666660308837891, 0.9966356960910244)
    # (0.006666819254557292, 0.9995334357884582)
    # (0.0066670576731363935, 0.997445195934548)
    # (0.008338292439778646, 0.9949822540916969)
    # (0.008571318217686244, 0.9994149608354542)
    # (0.008571522576468331, 0.9973152643729134)
    # (0.009230320270244893, 0.9999959349693333)
    # (0.009524027506510416, 0.9927804718880203)
    # (0.009526298159644717, 0.9998595268843181)
    # (0.009584835597446986, 0.9965213336043918)
    # (0.009973264875866118, 0.9934454063321695)
    # (0.010476180485316686, 0.9952736885979335)
    # (0.010476180485316686, 0.9958567046830272)
    # (0.010476555143083845, 0.9925354627447689)
    # (0.011428424290248327, 0.994727006187826)
    # (0.011428424290248327, 0.998279133846865)
    # (0.012162344796316964, 0.9970314898678718)
    # (0.012380940573556083, 0.9964432959002095)
    # (0.012382268905639648, 0.995712265607568)
    # (0.012385368347167969, 0.9963825537917568)
    # (0.013333320617675781, 0.9985319145953723)
    # (0.013720739455450149, 0.9958967409409618)
    # (0.014285530362810408, 0.9890169088433011)
    # (0.014285530362810408, 0.998332655090537)
    # (0.01428570066179548, 0.9905616759823475)
    # (0.014285870960780553, 0.9979451062908696)
    # (0.014286041259765625, 0.9971886559640882)
    # (0.01428774424961635, 0.998569818763703)
    # (0.014813968113490514, 0.997895076228652)
    # (0.014894962310791016, 0.9988524509833324)
    # (0.015045483907063803, 0.9926088254827263)
    # (0.015211559477306548, 0.9940099354106099)
    # (0.015232631138392858, 0.9997518027953466)
    # (0.015236445835658483, 0.993605900124191)
    # (0.01618988173348563, 0.9937262148314803)
    # (0.016190460750034878, 0.9935630871976295)
    # (0.016190653755551294, 0.9996581401475793)
    # (0.016190846761067707, 0.995684501453023)
    # (0.016190846761067707, 0.9994666643554847)
    # (0.016191039766584123, 0.9958815788091364)
    # (0.01619374184381394, 0.9969458718959404)
    # (0.017139366694859097, 0.9979666473843504)
    # (0.01714181900024414, 0.994362694522365)
    # (0.017142432076590403, 0.9985747135897939)
    # (0.017142840794154575, 0.9978985745492721)
    # (0.017142840794154575, 0.997992713054825)
    # (0.01714611053466797, 0.9983082912503554)
    # (0.018095220838274275, 0.9998191272397224)
    # (0.018096730822608584, 0.9977131238226993)
    # (0.019047827947707402, 0.9978743336979836)
    # (0.019517171950567336, 0.9949227676071613)
    # (0.021311214991978238, 0.9996334137633288)