 # -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 10:29:29 2021
用 tf2 写一个从 任意态 态制备到 Bell 态的 DQN 算法
@author: Waikikilick
"""

import  os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2' #设置 tf 的报警等级
import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers,optimizers,losses,initializers,Sequential,metrics,models
import copy 
from collections import deque
import random
from scipy.linalg import expm
from time import *

tf.random.set_seed(1)
np.random.seed(1)
random.seed (1)

class Agent(object):
    def __init__(self, 
            n_actions = 25,
            n_features = 8,
            learning_rate = 0.0001,
            reward_decay = 0.9,
            e_greedy = 0.95,
            replace_target_iter = 200,
            memory_size = 4096,
            batch_size = 32,
            e_greedy_increment = None):
        
        self.n_actions = n_actions
        self.n_features = n_features
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon_max = e_greedy
        self.replace_target_iter = replace_target_iter
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.epsilon_increment = e_greedy_increment
        self.epsilon = 0 if self.epsilon_increment is not None else self.epsilon_max
        self.learn_step_counter = 0
        self.memory = deque(maxlen=self.memory_size)
        self.memory_counter = 0
        
    def create_model(self): # 创建模型
    
        model = tf.keras.Sequential() # 模型搭建
        model.add(tf.keras.layers.Dense(256, input_dim=self.n_features, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        model.add(tf.keras.layers.Dense(256, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        model.add(tf.keras.layers.Dense(128, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        model.add(tf.keras.layers.Dense(self.n_actions, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        
        model.compile(loss='mse',optimizer=tf.keras.optimizers.RMSprop(learning_rate=self.lr)) # 模型装配
        return model
        
    def save_models(self): # 保存模型
    
        self.model.save('dqn_tf2_two_qubit_to_Bell_saved_model')
        self.target_model.save('dqn_tf2_two_qubit_to_Bell_saved_target_model')
        print('models saved !')
           
    def store_transition(self, s, a, r, s_): # 保存 记忆单元
    
        self.memory.append((s, a, r, s_)) 
        self.memory_counter += 1

    def choose_action(self, observation, tanxin): # tanxin 的值代表着 预测动作的 策略选择
      # tanxin = 0 意味着完全随机取动作
      # tanxin = 0.5 意味着执行动态贪心策略
      # tanxin = 1 意味着完全靠网络选择动作
        
        if tanxin == 0: # tanxin = 0, 意味着完全随机选动作
            action = np.random.randint(0, self.n_actions)
        elif tanxin == 1: # tanxin = 1, 意味着全靠网络预测动作, 通常在测试阶段用 # 其他值的话就意味着动作选择概率处于动态调整中, 比如 tanxin = 0.5
            demo = tf.reshape(observation,(1,8))	
            # action = np.argmax(self.model.predict(demo)[0])
            action = np.argmax(self.model(demo))
        else:
            if np.random.uniform() < self.epsilon:
                demo = tf.reshape(observation,(1,8))	
                # action = np.argmax(self.model.predict(demo)[0])
                action = np.argmax(self.model(demo))
            else:
                action = np.random.randint(0, self.n_actions)
            
        return action        
        
        
    def learn(self):
        
        if self.learn_step_counter % self.replace_target_iter == 0: # 每 replace_target_iter 次学习, 更新 target 网络参数一次
            self.target_model.set_weights(self.model.get_weights()) # target 网络参数从 主网络完全复制而来
                    
        if self.memory_counter < self.batch_size: # 只有当 记忆库中的数据量 比 批大小 多的时候才开始学习
            return # 如果 if 成立, 那么 return 后面的 且与 if 同级的代码不再执行
                
        batch_memory = random.sample(self.memory, self.batch_size) # 从 记忆库 中随机选出 批大小 数量的样本
        s_batch = np.array([replay[0] for replay in batch_memory]) # 多行数组
        demo_s_batch = tf.reshape(s_batch,(self.batch_size,8))
        Q = self.model.predict(demo_s_batch)
        # Q = self.model(demo_s_batch)
        
        next_s_batch = np.array([replay[3] for replay in batch_memory])
        demo_next_s_batch = tf.reshape(next_s_batch,(self.batch_size,8))
        Q_next = self.target_model.predict(demo_next_s_batch)
        # Q_next = self.target_model(demo_next_s_batch)

        # 使用公式更新训练集中的Q值
        for i, replay in enumerate(batch_memory):
            _, a, reward, _ = replay
            Q[i][a] =   reward + self.gamma * np.amax(Q_next[i]) # Q 值计算法则
        history = self.model.fit(demo_s_batch, Q, verbose=0) # 传入网络进行训练 # history 是记录每次训练时的 损失函数 字典 
            
        self.epsilon = self.epsilon + self.epsilon_increment if self.epsilon < self.epsilon_max else self.epsilon_max
        self.learn_step_counter += 1
      

class env(object):
    def  __init__(self, 
        dt = np.pi / 2,
        noise_a = 0,
        ): 
        self.action_space = np.array(  [[1,1],
                                        [1,2],
                                        [1,3],
                                        [1,4],
                                        [1,5],
                                        [2,1],
                                        [2,2],
                                        [2,3],
                                        [2,4],
                                        [2,5],
                                        [3,1],
                                        [3,2],
                                        [3,3],
                                        [3,4],
                                        [3,5],
                                        [4,1],
                                        [4,2],
                                        [4,3],
                                        [4,4],
                                        [4,5],
                                        [5,1],
                                        [5,2],
                                        [5,3],
                                        [5,4],
                                        [5,5]] )
        self.n_actions = len(self.action_space)
        self.n_features = 8 #描述状态所用的长度
        self.target_psi =  np.mat([[1], [0], [0], [1]], dtype=complex)/np.sqrt(2) #最终的目标态为 |0>,  np.array([1,0,0,0])
        self.h_1 = 1
        self.h_2 = 1
        self.I = np.matrix(np.identity(2, dtype=complex))
        self.s_x = np.mat([[0, 1], [1, 0]], dtype=complex)
        self.s_z = np.mat([[1, 0], [0, -1]], dtype=complex)
        self.dt = dt
        self.training_set, self.validation_set, self.testing_set = self.psi_set()
        
        
    def psi_set(self):
        
        alpha_num = 4
        
        theta = [np.pi/8,np.pi/4,3*np.pi/8]
        theta_1 = theta
        theta_2 = theta
        theta_3 = theta
        
        alpha = np.linspace(0,np.pi*2,alpha_num,endpoint=False)
        alpha_1 = alpha
        alpha_2 = alpha
        alpha_3 = alpha
        alpha_4 = alpha
        
        psi_set = []#np.matrix([[0,0,0,0]],dtype=complex) #第一行用来占位，否则无法和其他行并在一起，在最后要注意去掉这一行
        for ii in range(3): #theta_1
            for jj in range(3): #theta_2
                for kk in range(3): #theta_3
                    for mm in range(alpha_num): #alpha_1
                        for nn in range(alpha_num): #alpha_2
                            for oo in range(alpha_num): #alpha_3
                                for pp in range(alpha_num): #alpha_4
                                    
                                    a_1_mo = np.cos(theta_1[ii])
                                    a_2_mo = np.sin(theta_1[ii])*np.cos(theta_2[jj])
                                    a_3_mo = np.sin(theta_1[ii])*np.sin(theta_2[jj])*np.cos(theta_3[kk])
                                    a_4_mo = np.sin(theta_1[ii])*np.sin(theta_2[jj])*np.sin(theta_3[kk])
                                    
                                    a_1_real = a_1_mo*np.cos(alpha_1[mm])
                                    a_1_imag = a_1_mo*np.sin(alpha_1[mm])
                                    a_2_real = a_2_mo*np.cos(alpha_2[nn])
                                    a_2_imag = a_2_mo*np.sin(alpha_2[nn])
                                    a_3_real = a_3_mo*np.cos(alpha_3[oo])
                                    a_3_imag = a_3_mo*np.sin(alpha_3[oo])
                                    a_4_real = a_4_mo*np.cos(alpha_4[pp])
                                    a_4_imag = a_4_mo*np.sin(alpha_4[pp])
                                    
                                    a_1_complex = a_1_real + a_1_imag*1j
                                    a_2_complex = a_2_real + a_2_imag*1j
                                    a_3_complex = a_3_real + a_3_imag*1j
                                    a_4_complex = a_4_real + a_4_imag*1j
                                    
                                    a_complex = np.mat([[ a_1_complex], [a_2_complex], [a_3_complex], [a_4_complex]])
                                    # psi_set = np.row_stack((psi_set,a_complex))
                                    psi_set.append(a_complex)
                                    
        # psi_set = np.array(np.delete(psi_set,0,axis=0)) # 删除矩阵的第一行
        random.shuffle(psi_set) #打乱顺序
    
        training_set = psi_set[0:256]
        validation_set = psi_set[256:512]
        testing_set = psi_set[512:]
        # testing_set = psi_set[512:]
        
        return training_set, validation_set, testing_set
        
    
    def reset(self, init_psi): # 在一个新的回合开始时，归位到开始选中的那个点上
        
        init_state = np.array(init_psi.real.tolist() + init_psi.imag.tolist()) # 实向量形式
        # np.array([1实，2实，1虚，2虚])
        return init_state
    
    
    def step(self, state, action, nstep):
        
        psi = np.mat((np.array([state[0:int(len(state) / 2)] + state[int(len(state) / 2):int(len(state))] * 1j]).T).squeeze(0))  # 从 实向量 变回 复矩阵 形式
        #matrix([[ 1实 + 1虚j],
        #        [ 2实 + 2虚j]])
        
        J_1, J_2 =  self.action_space[action,0], self.action_space[action,1]  # control field strength
        J_12 = J_1 * J_2 /2
        
        H =  (J_1*np.kron(self.s_z, self.I) + J_2*np.kron(self.I, self.s_z) + \
                        J_12/2*np.kron((self.s_z-self.I),(self.s_z-self.I)) + \
           self.h_1*np.kron(self.s_x,self.I) + self.h_2*np.kron(self.I,self.s_x))/2
        U = expm(-1j * H * self.dt) 
        psi = U * psi  # next state

        err = 10e-4
        fid = (np.abs(psi.H * self.target_psi) ** 2).item(0).real  
        rwd = fid
        
        done = (((1 - fid) < err) or nstep >= 20 * np.pi / self.dt)  

        #再将量子态的 psi 形式恢复到 state 形式。 # 因为网络输入不能为复数，否则无法用寻常基于梯度的算法进行反向传播
    
        state = np.array(psi.real.tolist() + psi.imag.tolist()) # 实向量形式

        return state, rwd, done, fid    

def training(ep_max): 
    
    training_set = env.training_set
    validation_set = env.validation_set
    
    for i in range(ep_max):
        
        training_init_psi = random.choice(training_set)
        fid_max = 0
        print('------------------------------------')
        print('训练中..., 当前回合为:', i)
        observation = env.reset(training_init_psi)
        nstep = 0
        
        while True:
            action = agent.choose_action(observation, 0.5) 
            observation_, reward, done, fid = env.step(observation, action, nstep)  
            nstep += 1
            fid_max = max(fid_max, fid)
            agent.store_transition(observation, action, reward, observation_)
            agent.learn()
            observation = observation_
                
            if done:
                break
            
            
        if i == 730: #% 5 == 0: # 每 x 个回合用验证集验证一下效果，动作全靠网络，保存最大保真度和最大奖励
            print('阶段验证一下, 请稍等...')
            network = agent.model
            net = network.trainable_variables
            
            validation_fid_list = []
            validation_reward_tot_list = []
            
            for validation_init_psi in validation_set:
                
                validation_fid_max = 0
                validation_reward_tot = 0
                
                observation = env.reset(validation_init_psi)
                nstep = 0
                while True:
                    action = agent_matrix(observation) 
                    observation_, reward, done, fid = env.step(observation, action, nstep)  
                    nstep += 1
                    validation_fid_max = max(validation_fid_max, fid)
                    validation_reward_tot = validation_reward_tot + reward * (agent.gamma ** nstep)
                    observation = observation_
                    
                    if done:
                        break
                validation_fid_list.append(validation_fid_max)                
                validation_reward_tot_list.append(validation_reward_tot)
            
            validation_reward_history.append(np.mean(validation_reward_tot_list))
            validation_fid_history.append(np.mean(validation_fid_list))
            
            print('\n本回合验证集平均保真度: ', np.mean(validation_fid_list))
            # print('\n本回合验证集平均总奖励: ', np.mean(validation_reward_tot_list))
            

def relu(vector):
    for i in range(len(vector)):
        vector[i] = np.maximum(0,vector[i])
    return vector

def agent_matrix(x):
    out = np.mat(x).T * net[0].numpy() + net[1].numpy()
    out = relu(out)
    out = out * net[2].numpy() + net[3].numpy()
    out = relu(out)
    out = out * net[4].numpy() + net[5].numpy()
    out = relu(out)
    out = out * net[6].numpy() + net[7].numpy()
    out = relu(out)
    action = np.argmax(out)
    return action

def testing(): # 测试 测试集中的点 得到 保真度 分布
    print('\n测试中, 请稍等...')
    
    testing_set = env.testing_set
    fid_list = []
    
    for test_init_psi in testing_set:
        
        fid_max = 0
        observation = env.reset(test_init_psi)
        nstep = 0
        
        while True:
            action = agent_matrix(observation) 
            observation_, reward, done, fid = env.step(observation, action, nstep)  
            nstep += 1
            fid_max = max(fid_max, fid)
            observation = observation_
                
            if done:
                break
            
        fid_list.append(fid_max)
        
    return fid_list
                 
    
if __name__ == "__main__":
    
    dt = np.pi/2
    
    env = env(dt = dt)
    
    agent = Agent(n_actions = env.n_actions, 
                  n_features = env.n_features,
                  learning_rate = 0.001,
                  reward_decay = 0.9, 
                  e_greedy = 0.95,
                  replace_target_iter = 200,
                  memory_size = 40000,
                  e_greedy_increment = 0.0001)
    
    # agent.model = agent.create_model()
    # agent.target_model = agent.create_model() #选择直接新建网络，如果之前没有训练好的网络 或者 想忽略原有网络 就采用此项。此项和后面关于新建网络的代码块相冲突。
    
    # 如果当前工作目录已有之前训练好的模型，就直接导入，如果没有，就创建
    folder = os.getcwd() #返回当前工作目录
    files_list = os.listdir(folder) #用于返回指定的文件夹包含的文件或文件夹的名字的列表
    if 'dqn_tf2_two_qubit_to_Bell_saved_model_' in files_list: #判断是否已有训练好的网络
        agent.model = keras.models.load_model('dqn_tf2_two_qubit_to_Bell_saved_model') #导入训练好的网络 
        agent.target_model = keras.models.load_model('dqn_tf2_two_qubit_to_Bell_saved_target_model')
                
    else:
        agent.model = agent.create_model()
        agent.target_model = agent.create_model()
        
    network = agent.model
    net = network.trainable_variables
        
    validation_reward_history = []
    validation_fid_history = []
        
    begin_training = time()

    # 训练模块
    # (1) 此模块可反复利用, 首次利用时， 也就是直接在脚本里运行此程序. 需要再次使用以继续训练时，就复制到工作台执行。
    # (2) 当首次调用此模块, ep_max 应以 x * 5 - 1 结尾. 而在继续训练时 ep_max 应为 x * 5 + 1, 以避免记录 保真度 和 奖励值 时出现混乱, 其中 x 为整数
    training(ep_max = 731)   
   
    end_training = time()
    
    training_time = end_training - begin_training
    
    print('\ntraining_time =',training_time)  
        
    print('各验证回合回合奖励记录为: ', validation_reward_history)
    print('各验证回合最大保真度记录为: ', validation_fid_history)
    

# # # 测试
#     time1 = time()
#     testing_fid_list = testing()
#     time2 = time()
#     print('测试集平均保真度为：', np.mean(testing_fid_list))
#     print('测试时间所用为: ', time2 - time1)

# # 继续训练
# training(ep_max = )
# print('各验证回合回合奖励记录为: ', validation_reward_history)
# print('各验证回合最大保真度记录为: ', validation_fid_history)

# # 保存模型
# agent.save_models() 

# 各验证回合回合奖励记录为:  193*5 7.226213088216738
# [2.7886195056306127, 2.8070218066832138, 3.590677244157483, 3.9351669495757275, 4.271015403057653, 4.700182228921696, 5.023185063857466, 5.139956797395978, 5.354376322956373, 4.99082863971171, 5.070929185397644, 5.342115737463564, 5.1967785438810665, 5.5840349856365465, 5.36722073895161, 5.524180652267891, 5.729016539397444, 5.377175531921156, 5.675320346481147, 6.015647914064761, 5.793287507948256, 5.871491123589292, 5.582225480507938, 5.789887214253545, 5.9310282103128795, 5.873137306580593, 5.884809070880153, 6.19055984294339, 6.315885493948146, 6.256182347235389, 6.435473755235722, 6.29132177043076, 6.114745976810416, 6.6306705033029925, 6.119887322923482, 6.47292435562402, 6.086471694821146, 6.515127759075169, 6.63310966712408, 6.059096311871471, 6.363802628802038, 6.453738570716142, 6.233547669174848, 6.385355098600076, 6.761349576131198, 6.838220894305788, 6.196674446926105, 6.548822961766477, 6.918520969274323, 6.917026826934469, 6.662135780646043, 6.8086368144920275, 6.804233347595325, 6.886864309644524, 6.57726525671335, 6.783274295461288, 6.730395283528523, 6.876967866789251, 6.82508710249547, 6.89262468838341, 6.834997910977172, 6.947172871844142, 6.960931089673134, 6.897493938276629, 6.844459459607423, 6.879264695786715, 6.867707860262276, 6.917801397100873, 6.894942646208067, 6.861217262421729, 7.0112749293983185, 6.966836878997731, 6.877082344307633, 7.011497542182023, 6.995895536204827, 6.953828073854405, 7.015615354148151, 7.014352001755411, 6.885542011811235, 6.944344952171044, 6.930927018735989, 7.0777793411100856, 6.94788419016546, 6.6378365199310245, 7.028041962711539, 7.052623563053709, 6.985212778934703, 7.050138334339591, 7.084067871099805, 7.102649371006461, 6.977744621401946, 7.083316365943144, 6.99278220588919, 6.990406350992888, 6.992543169206458, 7.03380318657614, 7.0870299599170385, 7.039328069877528, 6.791785303491787, 7.032545698153731, 6.992402276798391, 6.99747136631232, 6.888998626569384, 7.017462402076582, 7.044834496587773, 7.10070164043304, 7.0418979805115764, 7.088635760421188, 6.932444261812949, 6.945138047203763, 6.988507947420907, 7.066363852114714, 7.061298077129187, 6.9702392407876195, 7.130580979684135, 7.034550565713587, 7.131149470620583, 6.920982724819718, 7.072197390172815, 7.132384755323515, 6.983931814887783, 7.025489820340615, 7.153083915379941, 7.002733045264133, 7.058416360760211, 6.997061933569718, 6.948968615245979, 7.115147843549812, 6.924878043529006, 7.06117755197973, 7.131291507984274, 7.082326287556603, 7.0848382813083575, 7.098084336256518, 6.9139501625297415, 6.908945249763718, 7.172820900203176, 7.149617451223646, 7.175598217358498, 7.084906504097091, 7.1136549453910005, 7.119320988922915, 7.036626535886082, 7.066575673164843, 7.05185982964686, 7.029366561693566, 6.999697970068645, 7.173031901979853, 7.144432024081997, 7.1473162807125945, 6.999173682361033, 7.070684896332889, 6.982138798196351, 7.131942354717026, 7.152373144845837, 7.0919339947459, 7.144702328477717, 7.196152355382555, 7.178134443642755, 7.070217180973135, 7.16908290308595, 7.115927226195308, 7.189737541142947, 7.107381548431748, 7.06446007452815, 7.11028297198595, 7.108387252861875, 7.21569082318835, 7.0621199834989365, 7.186126586009422, 7.173329895887596, 7.14312880411544, 7.158975785614155, 7.194880557153603, 7.215183399948953, 7.210377742241429, 7.207950802806884, 7.204611931079801, 7.118263622096844, 7.137802773744751, 7.172683199027434, 7.1264879838600415, 7.181194283999412, 7.216907826316557, 7.217449418958922, 7.1797936630826005, 7.161177493647984, 7.109631821215672, 7.076916511815031, 7.1583373477057, 7.131858012145118, 7.192429221526268, 7.146651376799515, 7.226213088216738, 7.154402203662034, 7.097343492187919, 7.205289896738067, 7.174458636898949, 7.17021698194218, 7.121275830448366, 7.185920776116628, 7.21546646789507, 7.0753665939115775, 7.200664682086092, 7.160244278563352, 7.180707373451334, 7.123200179904637, 7.146564952637285, 7.180301404673149]
# [2.78861951 2.80702181 3.59067724 3.93516695 4.2710154  4.70018223 5.02318506 5.1399568  5.35437632 4.99082864 5.07092919 5.34211574 5.19677854 5.58403499 5.36722074 5.52418065 5.72901654 5.37717553 5.67532035 6.01564791 5.79328751 5.87149112 5.58222548 5.78988721 5.93102821 5.87313731 5.88480907 6.19055984 6.31588549 6.25618235 6.43547376 6.29132177 6.11474598 6.6306705  6.11988732 6.47292436 6.08647169 6.51512776 6.63310967 6.05909631 6.36380263 6.45373857 6.23354767 6.3853551  6.76134958 6.83822089 6.19667445 6.54882296 6.91852097 6.91702683 6.66213578 6.80863681 6.80423335 6.88686431 6.57726526 6.7832743  6.73039528 6.87696787 6.8250871  6.89262469 6.83499791 6.94717287 6.96093109 6.89749394 6.84445946 6.8792647 6.86770786 6.9178014  6.89494265 6.86121726 7.01127493 6.96683688 6.87708234 7.01149754 6.99589554 6.95382807 7.01561535 7.014352 6.88554201 6.94434495 6.93092702 7.07777934 6.94788419 6.63783652 7.02804196 7.05262356 6.98521278 7.05013833 7.08406787 7.10264937 6.97774462 7.08331637 6.99278221 6.99040635 6.99254317 7.03380319 7.08702996 7.03932807 6.7917853  7.0325457  6.99240228 6.99747137 6.88899863 7.0174624  7.0448345  7.10070164 7.04189798 7.08863576 6.93244426 6.94513805 6.98850795 7.06636385 7.06129808 6.97023924 7.13058098 7.03455057 7.13114947 6.92098272 7.07219739 7.13238476 6.98393181 7.02548982 7.15308392 7.00273305 7.05841636 6.99706193 6.94896862 7.11514784 6.92487804 7.06117755 7.13129151 7.08232629 7.08483828 7.09808434 6.91395016 6.90894525 7.1728209  7.14961745 7.17559822 7.0849065  7.11365495 7.11932099 7.03662654 7.06657567 7.05185983 7.02936656 6.99969797 7.1730319  7.14443202 7.14731628 6.99917368 7.0706849  6.9821388  7.13194235 7.15237314 7.09193399 7.14470233 7.19615236 7.17813444 7.07021718 7.1690829  7.11592723 7.18973754 7.10738155 7.06446007 7.11028297 7.10838725 7.21569082 7.06211998 7.18612659 7.1733299  7.1431288  7.15897579 7.19488056 7.2151834  7.21037774 7.2079508  7.20461193 7.11826362 7.13780277 7.1726832  7.12648798 7.18119428 7.21690783 7.21744942 7.17979366 7.16117749 7.10963182 7.07691651 7.15833735 7.13185801 7.19242922 7.14665138 7.22621309 7.1544022  7.09734349 7.2052899  7.17445864 7.17021698 7.12127583 7.18592078 7.21546647 7.07536659 7.20066468 7.16024428 7.18070737 7.12320018 7.14656495 7.1803014 ]


# 各验证回合最大保真度记录为: 146*5 0.9697623783607289
# [0.6794255780526652, 0.7456564314450668, 0.8100478212339298, 0.8596447398052303, 0.8514365352767987, 0.8755403784275592, 0.8719761671673096, 0.87208223921233, 0.8817994421169888, 0.8589828152394655, 0.8421049991686174, 0.8354039077515951, 0.8144943939561222, 0.8509890406922598, 0.8458197786641976, 0.8623727705229958, 0.8646100976708909, 0.8267847537265332, 0.8762485684526379, 0.8996426607796784, 0.8906272976031142, 0.8621477364199299, 0.8833326736423972, 0.8904123251834414, 0.8935322110996387, 0.9013006454859775, 0.8764295134970783, 0.8909235846072778, 0.9189427555717471, 0.9292015465919673, 0.9270688672093959, 0.921310880301456, 0.8971906969813208, 0.9514870381246832, 0.8940120450178657, 0.9432775293604961, 0.9061160318142125, 0.9424052031295085, 0.9433520577948863, 0.888429321964039, 0.9093294828387655, 0.9307424044102826, 0.9141559582673441, 0.9249438996686271, 0.9503586124129971, 0.9461126074921322, 0.9362118264321097, 0.9318026411425884, 0.9612864124295509, 0.9518615417221403, 0.9560306013749637, 0.9568159974358748, 0.9576708497341391, 0.9539509457064579, 0.9477510190913327, 0.9552512662152262, 0.9421378706461527, 0.9586528079395478, 0.9524862081550929, 0.9506645326884493, 0.9520724498047735, 0.9609129990881946, 0.9614974356132983, 0.9592729509883102, 0.9592741131566223, 0.9580115101800515, 0.9624341920942818, 0.9579980333523286, 0.9380855864692714, 0.9509194360173876, 0.9633602573909938, 0.962964101795283, 0.9647250845939762, 0.9627666630080072, 0.9623814572489617, 0.9573042436352599, 0.9662162811562391, 0.9616416045218461, 0.943082466787422, 0.9668347135878583, 0.9529336021714123, 0.9625794776198309, 0.9573777587315537, 0.946066070757823, 0.9594747869361385, 0.9572769764940554, 0.9564138579089647, 0.9628474296959116, 0.9586011314154297, 0.9615962122890094, 0.9557303928754912, 0.962231725004939, 0.9638298081796985, 0.9576232717164544, 0.9674108566875836, 0.9562516135275023, 0.9612590246790557, 0.9593546418412875, 0.9329439568423014, 0.9637055216633508, 0.9651549299740017, 0.9559483849962671, 0.9606024942121493, 0.9601580881388045, 0.9644802374431074, 0.963946623991516, 0.961251946439373, 0.9643737583406298, 0.9628815965515138, 0.962691578202887, 0.9626953657715602, 0.9645312877375626, 0.969588633915233, 0.9619375905282819, 0.9629678290057422, 0.962198091567268, 0.9589671358667682, 0.9468910949986267, 0.9563081740272437, 0.9603760807514043, 0.9637299697537902, 0.9594238195292459, 0.9617369205561839, 0.9565384786492765, 0.9583318452123182, 0.9666287474525289, 0.9612925420712575, 0.9586647481185651, 0.9535758398498643, 0.963138565215224, 0.9614358204121882, 0.9622839618693589, 0.9666426621743092, 0.9593491046773039, 0.9592397659360645, 0.9494029696750623, 0.9588449097441052, 0.9625392391289191, 0.9628471107625289, 0.9634496793713823, 0.963960975167093, 0.9644600819286626, 0.959787371303459, 0.9568771479211695, 0.9618271553295723, 0.9622648888924537, 0.9697623783607289, 0.9648801154270954, 0.9646295259254177, 0.9601888146228286, 0.9572621099106033, 0.9554052829025426, 0.9593898672283341, 0.962317970739309, 0.9601521038526187, 0.9632208889359859, 0.9602246442254664, 0.9628259772685859, 0.9674370076094615, 0.9657516916398748, 0.9622918748937398, 0.9633440262376877, 0.9621546565754302, 0.9611401264450031, 0.966903521823752, 0.9601098656832252, 0.9652657015117839, 0.9614516531337269, 0.962906119954929, 0.9633369786488557, 0.964070553432683, 0.9652330609878345, 0.9612415292996317, 0.9636580173217544, 0.9600445016315917, 0.963223131366634, 0.9630117768768218, 0.9623736561986733, 0.9646723314061837, 0.9606613463460099, 0.9614837350785359, 0.960056735874019, 0.9603807916041303, 0.9626103274603655, 0.9658812407921076, 0.9646533894255034, 0.966921530524316, 0.9667446967319542, 0.9589304563504177, 0.9634507336601627, 0.9644534825439297, 0.9600839873695172, 0.9612258808726666, 0.9638929951169788, 0.9637960414405424, 0.9686878099510201, 0.9623553446698321, 0.9602539064144053, 0.9644091082349892, 0.9618127895667287, 0.9628277659829364, 0.9599185112466045, 0.960357628658971, 0.9636515199261275, 0.959157651845135, 0.9616839814234421, 0.9650451292922897, 0.9637457118334523, 0.9583349752365362]
# [0.67942558 0.74565643 0.81004782 0.85964474 0.85143654 0.87554038 0.87197617 0.87208224 0.88179944 0.85898282 0.842105   0.83540391 0.81449439 0.85098904 0.84581978 0.86237277 0.8646101  0.82678475 0.87624857 0.89964266 0.8906273  0.86214774 0.88333267 0.89041233 0.89353221 0.90130065 0.87642951 0.89092358 0.91894276 0.92920155 0.92706887 0.92131088 0.8971907  0.95148704 0.89401205 0.94327753 0.90611603 0.9424052  0.94335206 0.88842932 0.90932948 0.9307424 0.91415596 0.9249439  0.95035861 0.94611261 0.93621183 0.93180264 0.96128641 0.95186154 0.9560306  0.956816   0.95767085 0.95395095 0.94775102 0.95525127 0.94213787 0.95865281 0.95248621 0.95066453 0.95207245 0.960913   0.96149744 0.95927295 0.95927411 0.95801151 0.96243419 0.95799803 0.93808559 0.95091944 0.96336026 0.9629641 0.96472508 0.96276666 0.96238146 0.95730424 0.96621628 0.9616416 0.94308247 0.96683471 0.9529336  0.96257948 0.95737776 0.94606607 0.95947479 0.95727698 0.95641386 0.96284743 0.95860113 0.96159621 0.95573039 0.96223173 0.96382981 0.95762327 0.96741086 0.95625161 0.96125902 0.95935464 0.93294396 0.96370552 0.96515493 0.95594838 0.96060249 0.96015809 0.96448024 0.96394662 0.96125195 0.96437376 0.9628816  0.96269158 0.96269537 0.96453129 0.96958863 0.96193759 0.96296783 0.96219809 0.95896714 0.94689109 0.95630817 0.96037608 0.96372997 0.95942382 0.96173692 0.95653848 0.95833185 0.96662875 0.96129254 0.95866475 0.95357584 0.96313857 0.96143582 0.96228396 0.96664266 0.9593491  0.95923977 0.94940297 0.95884491 0.96253924 0.96284711 0.96344968 0.96396098 0.96446008 0.95978737 0.95687715 0.96182716 0.96226489 0.96976238 0.96488012 0.96462953 0.96018881 0.95726211 0.95540528 0.95938987 0.96231797 0.9601521  0.96322089 0.96022464 0.96282598 0.96743701 0.96575169 0.96229187 0.96334403 0.96215466 0.96114013 0.96690352 0.96010987 0.9652657  0.96145165 0.96290612 0.96333698 0.96407055 0.96523306 0.96124153 0.96365802 0.9600445  0.96322313 0.96301178 0.96237366 0.96467233 0.96066135 0.96148374 0.96005674 0.96038079 0.96261033 0.96588124 0.96465339 0.96692153 0.9667447  0.95893046 0.96345073 0.96445348 0.96008399 0.96122588 0.963893   0.96379604 0.96868781 0.96235534 0.96025391 0.96440911 0.96181279 0.96282777 0.95991851 0.96035763 0.96365152 0.95915765 0.96168398 0.96504513 0.96374571 0.95833498]

# 测试集平均保真度为： 0.969549466754459
# 测试时间所用为:  341.23155093193054

