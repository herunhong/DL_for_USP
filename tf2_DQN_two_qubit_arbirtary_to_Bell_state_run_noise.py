 # -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 10:29:29 2021
用 tf2 写一个从 任意态 态制备到 Bell 态的 DQN 算法
@author: Waikikilick
"""

import  os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2' #设置 tf 的报警等级
import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers,optimizers,losses,initializers,Sequential,metrics,models
import copy 
from collections import deque
import random
from scipy.linalg import expm
from time import *

tf.random.set_seed(1)
np.random.seed(1)
random.seed (1)

class Agent(object):
    def __init__(self, 
            n_actions = 25,
            n_features = 8,
            learning_rate = 0.0001,
            reward_decay = 0.9,
            e_greedy = 0.95,
            replace_target_iter = 200,
            memory_size = 4096,
            batch_size = 32,
            e_greedy_increment = None):
        
        self.n_actions = n_actions
        self.n_features = n_features
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon_max = e_greedy
        self.replace_target_iter = replace_target_iter
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.epsilon_increment = e_greedy_increment
        self.epsilon = 0 if self.epsilon_increment is not None else self.epsilon_max
        self.learn_step_counter = 0
        self.memory = deque(maxlen=self.memory_size)
        self.memory_counter = 0
        
    def create_model(self): # 创建模型
    
        model = tf.keras.Sequential() # 模型搭建
        model.add(tf.keras.layers.Dense(256, input_dim=self.n_features, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        model.add(tf.keras.layers.Dense(256, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        model.add(tf.keras.layers.Dense(128, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        model.add(tf.keras.layers.Dense(self.n_actions, activation='relu',kernel_initializer=initializers.RandomNormal(mean=0.0, stddev=0.05), bias_initializer=initializers.Constant(value=0.1)))
        
        model.compile(loss='mse',optimizer=tf.keras.optimizers.RMSprop(learning_rate=self.lr)) # 模型装配
        return model
        
    def save_models(self): # 保存模型
    
        self.model.save('dqn_tf2_two_qubit_to_Bell_saved_model')
        self.target_model.save('dqn_tf2_two_qubit_to_Bell_saved_target_model')
        print('models saved !')
           
    def store_transition(self, s, a, r, s_): # 保存 记忆单元
    
        self.memory.append((s, a, r, s_)) 
        self.memory_counter += 1

    def choose_action(self, observation): # tanxin 的值代表着 预测动作的 策略选择
        demo = tf.reshape(observation,(1,8))	
        action = np.argmax(self.model(demo))
            
        return action        
        
              

class env(object):
    def  __init__(self, 
        dt = np.pi / 2,
        noise_a = 0,
        ): 
        self.action_space = np.array(  [[1,1],
                                        [1,2],
                                        [1,3],
                                        [1,4],
                                        [1,5],
                                        [2,1],
                                        [2,2],
                                        [2,3],
                                        [2,4],
                                        [2,5],
                                        [3,1],
                                        [3,2],
                                        [3,3],
                                        [3,4],
                                        [3,5],
                                        [4,1],
                                        [4,2],
                                        [4,3],
                                        [4,4],
                                        [4,5],
                                        [5,1],
                                        [5,2],
                                        [5,3],
                                        [5,4],
                                        [5,5]] )
        self.n_actions = len(self.action_space)
        self.n_features = 8 #描述状态所用的长度
        self.target_psi =  np.mat([[1], [0], [0], [1]], dtype=complex)/np.sqrt(2) #最终的目标态为 |0>,  np.array([1,0,0,0])
        self.h_1 = 1
        self.h_2 = 1
        self.I = np.matrix(np.identity(2, dtype=complex))
        self.s_x = np.mat([[0, 1], [1, 0]], dtype=complex)
        self.s_z = np.mat([[1, 0], [0, -1]], dtype=complex)
        self.dt = dt
        self.training_set, self.validation_set, self.testing_set = self.psi_set()
        self.noise_normal_1 = np.array(
                  [ 1.62434536e+00, -6.11756414e-01, -5.28171752e-01, -1.07296862e+00,
                    8.65407629e-01, -2.30153870e+00,  1.74481176e+00, -7.61206901e-01,
                    3.19039096e-01, -2.49370375e-01,  1.46210794e+00, -2.06014071e+00,
                   -3.22417204e-01, -3.84054355e-01,  1.13376944e+00, -1.09989127e+00,
                   -1.72428208e-01, -8.77858418e-01,  4.22137467e-02,  5.82815214e-01,
                   -1.10061918e+00,  1.14472371e+00,  9.01590721e-01,  5.02494339e-01,
                    9.00855949e-01, -6.83727859e-01, -1.22890226e-01, -9.35769434e-01,
                   -2.67888080e-01,  5.30355467e-01, -6.91660752e-01, -3.96753527e-01,
                   -6.87172700e-01, -8.45205641e-01, -6.71246131e-01, -1.26645989e-02,
                   -1.11731035e+00,  2.34415698e-01,  1.65980218e+00,  7.42044161e-01,
                   -1.91835552e-01, -8.87628964e-01, -7.47158294e-01,  1.69245460e+00,
                    5.08077548e-02, -6.36995647e-01,  1.90915485e-01,  2.10025514e+00])
        
        self.noise_normal_2 = np.array(
                   [-4.16757847e-01, -5.62668272e-02, -2.13619610e+00,  1.64027081e+00,
                    -1.79343559e+00, -8.41747366e-01,  5.02881417e-01, -1.24528809e+00,
                    -1.05795222e+00, -9.09007615e-01,  5.51454045e-01,  2.29220801e+00,
                     4.15393930e-02, -1.11792545e+00,  5.39058321e-01, -5.96159700e-01,
                    -1.91304965e-02,  1.17500122e+00, -7.47870949e-01,  9.02525097e-03,
                    -8.78107893e-01, -1.56434170e-01,  2.56570452e-01, -9.88779049e-01,
                    -3.38821966e-01, -2.36184031e-01, -6.37655012e-01, -1.18761229e+00,
                    -1.42121723e+00, -1.53495196e-01, -2.69056960e-01,  2.23136679e+00,
                    -2.43476758e+00,  1.12726505e-01,  3.70444537e-01,  1.35963386e+00,
                     5.01857207e-01, -8.44213704e-01,  9.76147160e-06,  5.42352572e-01,
                    -3.13508197e-01,  7.71011738e-01, -1.86809065e+00,  1.73118467e+00,
                     1.46767801e+00, -3.35677339e-01,  6.11340780e-01,  4.79705919e-02])
    
        
        self.noise_constant = np.ones(41)
        
    def psi_set(self):
        
        alpha_num = 4
        
        theta = [np.pi/8,np.pi/4,3*np.pi/8]
        theta_1 = theta
        theta_2 = theta
        theta_3 = theta
        
        alpha = np.linspace(0,np.pi*2,alpha_num,endpoint=False)
        alpha_1 = alpha
        alpha_2 = alpha
        alpha_3 = alpha
        alpha_4 = alpha
        
        psi_set = []#np.matrix([[0,0,0,0]],dtype=complex) #第一行用来占位，否则无法和其他行并在一起，在最后要注意去掉这一行
        for ii in range(3): #theta_1
            for jj in range(3): #theta_2
                for kk in range(3): #theta_3
                    for mm in range(alpha_num): #alpha_1
                        for nn in range(alpha_num): #alpha_2
                            for oo in range(alpha_num): #alpha_3
                                for pp in range(alpha_num): #alpha_4
                                    
                                    a_1_mo = np.cos(theta_1[ii])
                                    a_2_mo = np.sin(theta_1[ii])*np.cos(theta_2[jj])
                                    a_3_mo = np.sin(theta_1[ii])*np.sin(theta_2[jj])*np.cos(theta_3[kk])
                                    a_4_mo = np.sin(theta_1[ii])*np.sin(theta_2[jj])*np.sin(theta_3[kk])
                                    
                                    a_1_real = a_1_mo*np.cos(alpha_1[mm])
                                    a_1_imag = a_1_mo*np.sin(alpha_1[mm])
                                    a_2_real = a_2_mo*np.cos(alpha_2[nn])
                                    a_2_imag = a_2_mo*np.sin(alpha_2[nn])
                                    a_3_real = a_3_mo*np.cos(alpha_3[oo])
                                    a_3_imag = a_3_mo*np.sin(alpha_3[oo])
                                    a_4_real = a_4_mo*np.cos(alpha_4[pp])
                                    a_4_imag = a_4_mo*np.sin(alpha_4[pp])
                                    
                                    a_1_complex = a_1_real + a_1_imag*1j
                                    a_2_complex = a_2_real + a_2_imag*1j
                                    a_3_complex = a_3_real + a_3_imag*1j
                                    a_4_complex = a_4_real + a_4_imag*1j
                                    
                                    a_complex = np.mat([[ a_1_complex], [a_2_complex], [a_3_complex], [a_4_complex]])
                                    # psi_set = np.row_stack((psi_set,a_complex))
                                    psi_set.append(a_complex)
                                    
        # psi_set = np.array(np.delete(psi_set,0,axis=0)) # 删除矩阵的第一行
        random.shuffle(psi_set) #打乱顺序
    
        training_set = psi_set[0:256]
        validation_set = psi_set[256:512]
        # testing_set = psi_set[512:1536]
        testing_set = psi_set[512:]
        
        return training_set, validation_set, testing_set
        
    
    def reset(self, init_psi): # 在一个新的回合开始时，归位到开始选中的那个点上
        
        init_state = np.array(init_psi.real.tolist() + init_psi.imag.tolist()) # 实向量形式
        # np.array([1实，2实，1虚，2虚])
        return init_state
    
    
    def step(self, state, action, nstep):
        
        psi = np.mat((np.array([state[0:int(len(state) / 2)] + state[int(len(state) / 2):int(len(state))] * 1j]).T).squeeze(0))  # 从 实向量 变回 复矩阵 形式
        #matrix([[ 1实 + 1虚j],
        #        [ 2实 + 2虚j]])
        
        J_1, J_2 =  self.action_space[action,0], self.action_space[action,1]  # control field strength
        J_12 = J_1 * J_2 /2
        
        H =  (J_1*np.kron(self.s_z, self.I) + J_2*np.kron(self.I, self.s_z) + \
                        J_12/2*np.kron((self.s_z-self.I),(self.s_z-self.I)) + \
           self.h_1*np.kron(self.s_x,self.I) + self.h_2*np.kron(self.I,self.s_x))/2
        U = expm(-1j * H * self.dt) 
        psi = U * psi  # next state

        err = 10e-4
        fid = (np.abs(psi.H * self.target_psi) ** 2).item(0).real  
        rwd = fid
        
        done = (((1 - fid) < err) or nstep >= 20 * np.pi / self.dt)  

        #再将量子态的 psi 形式恢复到 state 形式。 # 因为网络输入不能为复数，否则无法用寻常基于梯度的算法进行反向传播
    
        state = np.array(psi.real.tolist() + psi.imag.tolist()) # 实向量形式

        return state, rwd, done, fid    
    
    def step_noise_J(self, state, action, nstep):
        
        psi = np.mat((np.array([state[0:int(len(state) / 2)] + state[int(len(state) / 2):int(len(state))] * 1j]).T).squeeze(0))  # 从 实向量 变回 复矩阵 形式      
        J_1, J_2 =  self.action_space[action,0], self.action_space[action,1]  # control field strength
        J_1 += self.noise_1[nstep]
        J_2 += self.noise_2[nstep]
        J_12 = J_1 * J_2 /2
        
        H =  (J_1*np.kron(self.s_z, self.I) + J_2*np.kron(self.I, self.s_z) + \
                        J_12/2*np.kron((self.s_z-self.I),(self.s_z-self.I)) + \
           self.h_1*np.kron(self.s_x,self.I) + self.h_2*np.kron(self.I,self.s_x))/2
        U = expm(-1j * H * self.dt) 
        psi = U * psi  # next state
        fid = (np.abs(psi.H * self.target_psi) ** 2).item(0).real      
        state = np.array(psi.real.tolist() + psi.imag.tolist()) # 实向量形式

        return state, fid    
    
    def step_noise_h(self, state, action, nstep):
        
        psi = np.mat((np.array([state[0:int(len(state) / 2)] + state[int(len(state) / 2):int(len(state))] * 1j]).T).squeeze(0))  # 从 实向量 变回 复矩阵 形式      
        J_1, J_2 =  self.action_space[action,0], self.action_space[action,1]  # control field strength
        J_12 = J_1 * J_2 /2
        h_1 = self.h_1 + self.noise_1[nstep] #加入噪声1
        h_2 = self.h_2 + self.noise_2[nstep] #加入噪声2
        
        H =  (J_1*np.kron(self.s_z, self.I) + J_2*np.kron(self.I, self.s_z) + \
                        J_12/2*np.kron((self.s_z-self.I),(self.s_z-self.I)) + \
           h_1*np.kron(self.s_x,self.I) + h_2*np.kron(self.I,self.s_x))/2
        U = expm(-1j * H * self.dt) 
        psi = U * psi  # next state
        fid = (np.abs(psi.H * self.target_psi) ** 2).item(0).real      
        state = np.array(psi.real.tolist() + psi.imag.tolist()) # 实向量形式

        return state, fid    


def testing(): # 测试 测试集中的点 得到 保真度 分布
    print('\n测试中, 请稍等...')
    
    testing_set = env.testing_set
    fid_list = []
    
    for test_init_psi in testing_set:
        
        fid_max = 0
        observation = env.reset(test_init_psi)
        nstep = 0
        
        while True:
            action = agent.choose_action(observation) 
            observation_, reward, done, fid = env.step(observation, action, nstep)  
            nstep += 1
            fid_max = max(fid_max, fid)
            observation = observation_
                
            if done:
                break
            
        fid_list.append(fid_max)
        
    return fid_list

def testing_noise_J_dynamic(noise_a): # 测试 测试集中的点 得到 保真度 分布
    print('\n测试 J 动态噪声中, 请稍等...')
    print('噪声振幅为：', noise_a)
    env.noise_1 = noise_a * env.noise_normal_1
    env.noise_2 = noise_a * env.noise_normal_2
    fid_noise_list = []
    
    for test_init_psi in testing_set:
        
        fid_max = 0
        observation = env.reset(test_init_psi)
        nstep = 0
        action_list = [] #用来保存本回合所采取的动作，用于噪声分析
        fid_list = [] #用来保存本回合中的保真度，选择最大保真度对应的步骤作为后面噪声环境中动作的终止步骤
        
        while True:
            action = agent.choose_action(observation) 
            observation_, reward, done, fid = env.step(observation, action, nstep) 
            action_list.append(action)
            fid_list.append(fid)
            nstep += 1
            fid_max = max(fid_max, fid)
            observation = observation_
                
            if done:
                break
            
        max_index = fid_list.index(max(fid_list))
        action_list = action_list[0:max_index+1]
        
        observation = env.reset(test_init_psi)
        
        # 加入噪声
        nstep = 0
        for action in action_list:
            
            observation_, fid = env.step_noise_J(observation, action, nstep)
            observation = observation_
            nstep += 1
             #选择最后一步的保真度作为本回合的保真度
            
        fid_noise_list.append(fid) # 将最终保真度记录到矩阵中
            
    return fid_noise_list   

def testing_noise_J_static(noise_a): # 测试 测试集中的点 得到 保真度 分布
    print('\n测试 J 静态噪声中, 请稍等...')
    print('噪声振幅为：', noise_a)
    env.noise_1 = noise_a * env.noise_constant
    env.noise_2 = noise_a * env.noise_constant
    fid_noise_list = []
    
    for test_init_psi in testing_set:
        
        fid_max = 0
        observation = env.reset(test_init_psi)
        nstep = 0
        action_list = [] #用来保存本回合所采取的动作，用于噪声分析
        fid_list = [] #用来保存本回合中的保真度，选择最大保真度对应的步骤作为后面噪声环境中动作的终止步骤
        
        while True:
            action = agent.choose_action(observation) 
            observation_, reward, done, fid = env.step(observation, action, nstep) 
            action_list.append(action)
            fid_list.append(fid)
            nstep += 1
            fid_max = max(fid_max, fid)
            observation = observation_
                
            if done:
                break
            
        max_index = fid_list.index(max(fid_list))
        action_list = action_list[0:max_index+1]
        
        observation = env.reset(test_init_psi)
        
        # 加入噪声
        nstep = 0
        for action in action_list:
            
            observation_, fid = env.step_noise_J(observation, action, nstep)
            observation = observation_
            nstep += 1
             #选择最后一步的保真度作为本回合的保真度
            
        fid_noise_list.append(fid) # 将最终保真度记录到矩阵中
            
    return fid_noise_list   

def testing_noise_h_dynamic(noise_a): # 测试 测试集中的点 得到 保真度 分布
    print('\n测试 h 动态噪声中, 请稍等...')
    print('噪声振幅为：', noise_a)
    env.noise_1 = noise_a * env.noise_normal_1
    env.noise_2 = noise_a * env.noise_normal_2
    fid_noise_list = []
    
    for test_init_psi in testing_set:
        
        fid_max = 0
        observation = env.reset(test_init_psi)
        nstep = 0
        action_list = [] #用来保存本回合所采取的动作，用于噪声分析
        fid_list = [] #用来保存本回合中的保真度，选择最大保真度对应的步骤作为后面噪声环境中动作的终止步骤
        
        while True:
            action = agent.choose_action(observation) 
            observation_, reward, done, fid = env.step(observation, action, nstep) 
            action_list.append(action)
            fid_list.append(fid)
            nstep += 1
            fid_max = max(fid_max, fid)
            observation = observation_
                
            if done:
                break
            
        max_index = fid_list.index(max(fid_list))
        action_list = action_list[0:max_index+1]
        
        observation = env.reset(test_init_psi)
        
        # 加入噪声
        nstep = 0
        for action in action_list:
            
            observation_, fid = env.step_noise_h(observation, action, nstep)
            observation = observation_
            nstep += 1
             #选择最后一步的保真度作为本回合的保真度
            
        fid_noise_list.append(fid) # 将最终保真度记录到矩阵中
            
    return fid_noise_list  

def testing_noise_h_static(noise_a): # 测试 测试集中的点 得到 保真度 分布
    print('\n测试 h 静态噪声中, 请稍等...')
    print('噪声振幅为：', noise_a)
    env.noise_1 = noise_a * env.noise_constant
    env.noise_2 = noise_a * env.noise_constant
    fid_noise_list = []
    
    for test_init_psi in testing_set:
        
        fid_max = 0
        observation = env.reset(test_init_psi)
        nstep = 0
        action_list = [] #用来保存本回合所采取的动作，用于噪声分析
        fid_list = [] #用来保存本回合中的保真度，选择最大保真度对应的步骤作为后面噪声环境中动作的终止步骤
        
        while True:
            action = agent.choose_action(observation) 
            observation_, reward, done, fid = env.step(observation, action, nstep) 
            action_list.append(action)
            fid_list.append(fid)
            nstep += 1
            fid_max = max(fid_max, fid)
            observation = observation_
                
            if done:
                break
            
        max_index = fid_list.index(max(fid_list))
        action_list = action_list[0:max_index+1]
        
        observation = env.reset(test_init_psi)
        
        # 加入噪声
        nstep = 0
        for action in action_list:
            
            observation_, fid = env.step_noise_h(observation, action, nstep)
            observation = observation_
            nstep += 1
             #选择最后一步的保真度作为本回合的保真度
            
        fid_noise_list.append(fid) # 将最终保真度记录到矩阵中
            
    return fid_noise_list           
    
if __name__ == "__main__":
    
    dt = np.pi/2
    
    env = env(dt = dt)
    
    agent = Agent(n_actions = env.n_actions, 
                  n_features = env.n_features,
                  learning_rate = 0.001,
                  reward_decay = 0.9, 
                  e_greedy = 0.95,
                  replace_target_iter = 200,
                  memory_size = 40000,
                  e_greedy_increment = 0.0001)
    
    agent.model = keras.models.load_model('dqn_tf2_two_qubit_to_Bell_saved_model') #导入训练好的网络 
    agent.target_model = keras.models.load_model('dqn_tf2_two_qubit_to_Bell_saved_target_model')
    
    testing_set = env.testing_set          

    
    # 测试 噪声 对 保真度 的影响
    
    fids_noise_J_Static_list = []
    fids_noise_h_Static_list = []
    fids_noise_J_Dynamic_list = []
    fids_noise_h_Dynamic_list = []
    
    start_time = time()    
    for noise_a in [-0.01, -0.009, -0.008, -0.007, -0.006, -0.005, -0.004, -0.003, -0.002, -0.001, 0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01]:
        test_noise_mean = np.mean(testing_noise_J_static(noise_a))
        end_time = time()
        print(' J 静态噪声下的平均保真度为：',test_noise_mean)
        print('所用时间为：', end_time - start_time)
        fids_noise_J_Static_list.append(test_noise_mean)
     
    start_time = time()  
    for noise_a in [-0.01, -0.009, -0.008, -0.007, -0.006, -0.005, -0.004, -0.003, -0.002, -0.001, 0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01]:
        test_noise_mean = np.mean(testing_noise_h_static(noise_a))
        end_time = time()
        print(' h 静态噪声下的平均保真度为：',test_noise_mean)
        print('所用时间为：', end_time - start_time)
        fids_noise_h_Static_list.append(test_noise_mean)
        
    start_time = time()  
    for noise_a in [0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]:
        test_noise_mean = np.mean(testing_noise_J_dynamic(noise_a))
        end_time = time()
        print(' J 动态噪声下的平均保真度为：',test_noise_mean)
        print('所用时间为：', end_time - start_time)
        fids_noise_J_Dynamic_list.append(test_noise_mean) 
    
    start_time = time()  
    for noise_a in [0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]:
        test_noise_mean = np.mean(testing_noise_h_dynamic(noise_a))
        end_time = time()
        print(' h 动态噪声下的平均保真度为：',test_noise_mean)
        print('所用时间为：', end_time - start_time)
        fids_noise_h_Dynamic_list.append(test_noise_mean) 
    
    
    print('fids_noise_J_Static_list', fids_noise_J_Static_list)
    print('fids_noise_h_Static_list', fids_noise_h_Static_list)
    
    print('fids_noise_J_Dynamic_list', fids_noise_J_Dynamic_list)
    print('fids_noise_h_Dynamic_list', fids_noise_h_Dynamic_list)
    
    # 1024 个测试点结果：
    # fids_noise_J_Static_list [0.955440389826011, 0.9580225304625387, 0.960363042244727, 0.9624544973356016, 0.9642896825972432, 0.9658616279949706, 0.967163635387029, 0.9681893075682393, 0.9689325774274999, 0.969387737070878, 0.969549466754459, 0.9694128634645843, 0.9689734689771943, 0.9682272972235801, 0.9671708607854155, 0.9658011963401781, 0.9641158888760841, 0.9621130944958494, 0.959791561630113, 0.9571506504836098, 0.9541903505418855]
    # fids_noise_h_Static_list [0.9689329385447929, 0.9691473178758077, 0.9693275674970633, 0.9694737475013158, 0.969585924544241, 0.9696641717751218, 0.9697085687674101, 0.9697192014488993, 0.9696961620319156, 0.9696395489432891, 0.969549466754459, 0.9694260261116597, 0.9692693436661647, 0.9690795420048741, 0.9688567495810897, 0.968601100645649, 0.9683127351784124, 0.9679917988202859, 0.9676384428054977, 0.9672528238946019, 0.966835104307906]
    # fids_noise_J_Dynamic_list [0.969549466754459, 0.9660144792597876, 0.9561552232704018, 0.9404651188807958, 0.9196033350691976, 0.8943495634751534, 0.8655550537874102, 0.8340941322164802, 0.8008200009293519, 0.7665278342564219, 0.7319271599850194]
    # fids_noise_h_Dynamic_list [0.969549466754459, 0.9691763947168269, 0.9677857761636206, 0.9654012553450695, 0.9620504641586368, 0.9577646375190945, 0.9525782171593458, 0.9465284497843287, 0.939654985089735, 0.9319994786821588, 0.9236052044159028]


    


